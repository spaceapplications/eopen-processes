# Cloudify HPC PoC

Executing an example job on HLRS HPC using Cloudify Manager.

To import this process in ASB:

```
tar czvf hpc-example.tar.gz hpc-example
```

And upload `hpc-example.tar.gz` alongside the `process_wrapper.py`.