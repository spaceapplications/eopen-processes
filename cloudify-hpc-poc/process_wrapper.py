#!/usr/bin/python

import ast
import json
import logging
import os
import time

import yaml
from cloudify_rest_client import CloudifyClient
from cloudify_rest_client.exceptions import CloudifyClientError

logger = logging.getLogger(__name__)
formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')
console_handler = logging.StreamHandler()
console_handler.setFormatter(formatter)
logger.setLevel(logging.DEBUG)
logger.addHandler(console_handler)

# FIXME Hardcoded credentials for cloudify. Better using token.
credentials = {
    'username': 'admin',
    'password': 'admin'
}

RUNNING_STATUSES = ['pending', 'started', 'cancelling', 'force_cancelling']
FAILED_STATUSES = ['failed']


def wait_for_running_executions(client, deployment_id):
    logger.info("Waiting for running executions of deployment %s to end..", deployment_id)
    has_running_executions = True
    while has_running_executions:
        has_running_executions = False
        executions = client.executions.list(deployment_id=deployment_id)
        logger.debug("Found %s executions: %s", len(executions), [e['workflow_id'] for e in executions])
        for execution in executions:
            logger.debug("Execution status: %s - %s", execution['workflow_id'], execution['status'])
            has_running_executions = has_running_executions or execution['status'] in RUNNING_STATUSES
        if has_running_executions:
            logger.debug("Sleeping 2 seconds..")
            time.sleep(2)


def remove_blueprint(client, blueprint_id):
    logger.info("Removing blueprint: %s", blueprint_id)
    try:
        client.blueprints.delete(blueprint_id=blueprint_id)
    except CloudifyClientError as e:
        if e.status_code == 404:
            logger.debug("Blueprint %s not found, nothing to remove", blueprint_id)
        else:
            raise e
    logger.info("Blueprint %s removed", blueprint_id)


def remove_deployment(client, deployment_id):
    logger.info("Removing deployment: %s", deployment_id)
    try:
        client.deployments.delete(deployment_id=deployment_id)
    except CloudifyClientError as e:
        if e.status_code == 404:
            logger.debug("Deployment %s not found, nothing to remove", deployment_id)
        else:
            raise e
    logger.info("Waiting for deployment executions (especially delete_deployment_environment) to finish..")
    wait_for_running_executions(client, deployment_id)


def remove_blueprint_and_deployment(client, blueprint_id, deployment_id):
    remove_deployment(client, deployment_id)
    remove_blueprint(client, blueprint_id)


def execute(out_dir, cloudify_manager_host='eopen-services', cloudify_manager_port='8010', name='hpc-poc',
            workflows='["install", "run_jobs", "uninstall"]', suffix_with_hostname='False', delete_if_exists='True',
            delete_when_finished='False', reuse_blueprint='False', fail_on_executions_error='True'):
    """
    Identification:
    Name -- Cloudify HPC PoC
    Description -- Run the sample provided by HLRS on HPC using Cloudify Manager
    Author -- Hakim Boulahya

    Inputs:
    cloudify_manager_host -- Cloudify Manager Host -- 45/User String -- eopen-services
    cloudify_manager_port -- Cloudify Manager Port -- 45/User String -- 8010
    name -- name -- 45/User String -- hpc-poc
    workflows -- Workflows -- 45/User String -- ["install", "run_jobs", "uninstall"]
    suffix_with_hostname -- Suffix With Hostname -- 45/User String -- False
    delete_if_exists -- Delete If Exists -- 45/User String -- True
    delete_when_finished -- Delete When Finished -- 45/User String -- False
    reuse_blueprint -- Reuse Blueprint -- 45/User String -- False
    fail_on_executions_error -- Fail On Executions Error -- 45/User String -- True

    Outputs:
    cloudify_log_dir -- Cloudify Log Dir -- 45/User String

    Main Dependency:
    python-2

    Software Dependencies:
    python-2
    cloudify_deps-x.y

    Processing Resources:
    ram -- 1
    disk -- 1
    cpu -- 1
    """

    # Validations
    workflows = ast.literal_eval(workflows)
    if not isinstance(workflows, tuple) and not isinstance(workflows, list):
        message = "workflows input must be list or tuple, received type %s" % workflows.__class__
        logger.error(message)
        raise ValueError(message)

    for var in ['suffix_with_hostname', 'delete_if_exists', 'delete_when_finished', 'reuse_blueprint',
                'fail_on_executions_error']:
        value = locals()[var].strip().lower().capitalize()
        if value not in ['True', 'False']:
            message = "%s input must be a string with value: 'True' of 'False', received '%s'." % (var, value)
            logger.error(message)
            raise ValueError(message)
        # How ugly is that ? I give it 8/10
        exec ("%s = %s" % (var, value))
    cloudify_log_dir = os.path.join(out_dir, 'cloudify_logs')
    if not os.path.exists(cloudify_log_dir):
        logger.debug("Creating directory %s", cloudify_log_dir)
        os.mkdir(cloudify_log_dir)
    logger.info('out_dir: %s', out_dir)
    logger.info("Cloudify Log Directory: %s", cloudify_log_dir)
    logger.info("Cloudify Manager host:port : %s:%s", cloudify_manager_host, cloudify_manager_port)
    logger.info('Name %s', name)
    logger.info("Workflows %s", workflows)
    logger.info("suffix_with_hostname (blueprint and deployment): %s", suffix_with_hostname)
    logger.info("delete_if_exists: %s", delete_if_exists)
    logger.info("delete_when_finished: %s", delete_when_finished)
    logger.info("reuse_blueprint: %s", reuse_blueprint)
    logger.info("fail_on_executions_error: %s", fail_on_executions_error)
    if suffix_with_hostname:
        name = name + os.getenv('HOSTNAME', '-unknown_host')
        logger.debug("Suffix with hostname enabled, new name: %s", name)
    blueprint_id, deployment_id = name, name + '-deployment'
    logger.info("Blueprint id: %s", blueprint_id)
    logger.info("Deployment id: %s", deployment_id)
    client = CloudifyClient(host=cloudify_manager_host, port=cloudify_manager_port,
                            tenant='default_tenant', **credentials)
    # If no env vars, considering that the script is run locally
    process_data_folder = os.getenv('PYWPS_PROCESSES', '.')
    blueprint_file = os.path.join(process_data_folder, 'hpc-example', 'blueprint.yaml')
    inputs_file = os.path.join(process_data_folder, 'hpc-example', 'inputs.yaml')
    logger.info("Blueprint file: %s", blueprint_file)
    logger.info("Inputs file: %s", inputs_file)

    if delete_if_exists:
        logger.info("delete_if_exists enabled, removing deployment and blueprint (if not reusing)")
        remove_deployment(client, deployment_id)
        if not reuse_blueprint:
            remove_blueprint(client, blueprint_id)
    logger.info("Uploading blueprint %s with file: %s", blueprint_id, blueprint_file)
    if not reuse_blueprint:
        with open(blueprint_file) as f:
            logger.debug("Blueprint file %s content:\n\n%s", blueprint_file, ''.join(f.readlines()))
        # FIXME uploading the file like that fails. It seems that it comes from the fact that cloudify_rest_client
        # FIXME tar the files first, which might change the permissions when reaching the server.
        client.blueprints.upload(blueprint_file, blueprint_id)
    with open(inputs_file) as f:
        inputs = yaml.load(f)
    logger.info("Creating deployment %s with inputs %s", deployment_id, inputs_file)
    client.deployments.create(blueprint_id=blueprint_id, deployment_id=deployment_id,
                              inputs=inputs, skip_plugins_validation=True)
    wait_for_running_executions(client, deployment_id)
    logger.info("Running following worflows (in order): %s", workflows)
    for workflow_id in workflows:
        logger.info("Running workflow: %s", workflow_id)
        client.executions.start(deployment_id=deployment_id, workflow_id=workflow_id)
        wait_for_running_executions(client, deployment_id)

    blueprint_log_file = os.path.join(cloudify_log_dir, 'blueprint-%s.json' % blueprint_id)
    deployment_log_file = os.path.join(cloudify_log_dir, 'deployment-%s.json' % deployment_id)
    with open(blueprint_log_file, 'w') as f:
        logger.info("Writing file %s" % blueprint_log_file)
        json.dump(client.blueprints.get(blueprint_id), f, indent=2)
    with open(deployment_log_file, 'w') as f:
        json.dump(client.deployments.get(deployment_id), f, indent=2)
    executions = client.executions.list(deployment_id=deployment_id)
    for execution in executions:
        execution_log_file = os.path.join(cloudify_log_dir, 'execution-%s-%s.json' % (execution['workflow_id'],
                                                                                      execution['id']))
        with open(execution_log_file, 'w') as f:
            logger.info("Writing file %s", execution_log_file)
            json.dump(execution, f, indent=2)

    if delete_when_finished:
        logger.info("delete_when_finished is enabled")
        remove_deployment(client, deployment_id)
        if not reuse_blueprint:
            remove_blueprint(client, blueprint_id)

    if fail_on_executions_error:
        for execution in executions:
            if execution['status'] in FAILED_STATUSES:
                raise RuntimeError("Execution %s (id: %s) failed with status %s" % (execution['workflow_id'],
                                                                                    execution['id'],
                                                                                    execution['status']))

    return {
        'cloudify_log_dir': cloudify_log_dir
    }


def main():
    cloudify_manager_host = 'cloudify-host'
    execute('/tmp', cloudify_manager_host, '8010', name='hpc-poc-local',
            workflows='["install", "run_jobs", "uninstall"]', suffix_with_hostname='False',
            delete_if_exists='True', delete_when_finished='False', reuse_blueprint='False',
            fail_on_executions_error='True')


if __name__ == "__main__":
    main()
